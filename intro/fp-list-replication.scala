def f(num: Int, arr: List[Int]) =
  arr.flatMap { e => (1 to num).map(_ => e).toList }
