import scala.io.Source

object Solution extends App {
  println(Source.stdin.getLines().take(2).map(_.toInt).sum)
}
